<?php

include_once("classes/ville.php");
include_once("classes/joueur.php");
include_once("classes/carte.php");

$ville = new Ville("Le Mans", 2000, 800, 1000, 0, 200, 400, 2);
var_dump($ville);
$ville2 = new Ville("Angers", 1050, 500, 1000, 0, 200, 400, 2);
var_dump($ville2);
echo "<br>";
$joueur = new Joueur("Matthieu", [], 150000, []);
var_dump($joueur);
echo "<br>";

$carte = new Carte("Carte chance", "Recevez 500");
var_dump($carte);
echo "<br>";

$joueur->achatVille($ville);
$joueur->achatVille($ville2);
var_dump($joueur);
echo "<br>";

$joueur->hypotheque($ville);
var_dump($joueur);
echo "<br>";

$joueur->loyer($ville, $joueur);
var_dump($joueur);
echo "<br>";

$joueur->piocher($carte);
var_dump($joueur);
echo "<br><p> Résultat </p>";

var_dump($joueur);
