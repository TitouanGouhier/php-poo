<?php

class Joueur
{
  public $nomJoueur;
  public $villeJoueur = [];
  public $argent;
  public $carteJoueur = [];
  public $double = 0;

  public function __construct($nomJoueur, $villeJoueur, $argent, $carteJoueur)
  {
    $this->nomJoueur = $nomJoueur;
    $this->villeJoueur = $villeJoueur;
    $this->argent = $argent;
    $this->carteJoueur = $carteJoueur;
  }
//Le lancé de dé
  public function lancerDe() {
    $de1 = rand(1, 6);
    $de2 = rand(1, 6);
    if ($de1 == $de2) {
      $this->double ++;
      echo "Rejouez";
      if ($this->double == 3) {
        echo "ça pars en cellule.";
      }
    }
    else {
      echo "Avancez de " . ($de1 + $de2) . " cases.";
      $this->double = 0;
    }
  }
//Un joueur achete une ville
  public function achatVille($ville) {
    if ($ville->statut==0 && $this->argent>=$ville->prixAchat) {
      $this->argent -= $ville->prixAchat;
      $this->villeJoueur[$ville->couleurGrp][] = $ville;
      $ville->statut = 1;
    }
    elseif ($ville->statut==0 && $this->argent<$ville->prixAchat) {
      echo ($this->nomJoueur . "Ca manque d'argent.");
    }
    else {
      echo "Vous ne pouvez pas acheter d'argent";
    }
  }
//Un joueur vend une ville
  public function hypotheque($ville) {
    if (in_array($ville, $this->villeJoueur) && $ville->hypotheque==0) {
      $this->argent += $ville->prixVente;
      $ville->hypotheque = 1;
    }
    elseif (in_array($ville, $this->villeJoueur) && $ville->hypotheque==1) {
      echo "Ville deja vendu";
    }
    else {
      echo "Vous ne pouvez pas vendre la ville";
    }
  }
//Gerer le loyer
  public function loyer($ville, $joueur) {
    if ($ville->statut==1) {
      if (in_array($ville, $this->villeJoueur)) {
        echo "La ville est a vous";
      }
      else {
        $this->argent -= $ville->loyer;
        $joueur->argent += $ville->loyer;
      }
    }
    else {
      echo "La ville est a personne";
    }
  }
//Piocher une carte
  public function piocher($carte) {
    if ($carte->pioche == 0) {
      $this->carteJoueur[] = $carte;
      $carte->pioche = 1;
    }
  }
}
